package bandurria_hero;

import java.util.Scanner;

public class bandurria {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int cas = sc.nextInt();
		sc.nextLine();
		for (int i = 0; i<cas; i++) {
			bandurria(sc.nextLine());
			}
	}
	
	public static int bandurria(String linea) {
		int combo = 10;
			String a[] = linea.split("");
			int count = 0;
			int puntos = 0;
			for (String string : a) {
				if(string.equals("O")) {
					count++;
					puntos += combo*count;
				}else {
					count = 0;
				}
			}
			return puntos;
		
	}
}
