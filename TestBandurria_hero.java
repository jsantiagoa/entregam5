package bandurria_hero;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestBandurria_hero {

	@Test
	void Public() {
		assertEquals(30, bandurria.bandurria("OO"));
		assertEquals(20, bandurria.bandurria("O.O."));
		assertEquals(100, bandurria.bandurria("OOOO"));

	}
	@Test
	void Privat() {
		assertEquals(0, bandurria.bandurria("."));
		assertEquals(10, bandurria.bandurria("O"));
		assertEquals(60, bandurria.bandurria("OO.OO"));
	}

}
