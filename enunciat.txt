
Ivan és aficionat als jocs de ritme i orgullós posseïdor de relíquies com Guitar Hero, Rock Band o Guitar Master, amb tots els trastos de les diferents plataformes en què es van publicar.

Però la seva veritable afició és la bandúrria. Poder tocar a la consola de videojocs cançons clàssiques de la tuna com "Clavelitos", "Cillet valent" o "De colors" li faria recordar altres paisatges i altres temps… i aconseguir molts més punts que amb qualsevol altre joc.

De manera que, ni curt ni mandrós, està aprenent a programar per poder fer-se el seu propi Bandurria Hero. De moment, està encallat amb el càlcul de la puntuació per culpa dels combos. Vol que si aconsegueix tocar correctament una nota se sumin 10 punts. Si a continuació s'encerta amb la nota següent, aquesta es premiï amb 20 punts, la següent amb 30 i així successivament, incrementant la valoració de 10 en 10, sempre que no es falli. Quan es premi incorrectament una corda o se salti una nota, el combo s'acaba i la següent nota correcta tornarà a ser valorada amb 10 punts.

## Entrada

El programa haurà de llegir, de l'entrada estàndard, un primer número indicant quants casos de prova vindran a continuación.

Cada cas de prova consisteix en una línia composta d'una seqüència de "O" (vocal "o" majúscula) i "." indicant, respectivament, una nota correcta o una fallada. Tots els casos tenen almenys un caràcter i cap no en té més de 10.000.

## Sortida

Per cada cas de prova s'indicarà la puntuació que s'ha de donar al jugador tenint en compte la valoració dels combos, on la primera nota d'una sèrie consecutiva d'encerts es valora amb 10 punts i les notes següents de la sèrie proporcionen 10 punts més que l'anterior.

## Exemple d'entrada

```
3
OO.
O.O.
OOOO
```

## Exemple de Sortida

```
30
20
100
```